/*
 * SPDX-License-Identifier: BSD-2-Clause
 * 
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "project-defs.h"
#include <timer-hal.h>

/**
 * @file timer-hal.c
 * 
 * Timer abstraction implementation for STC12, STC15 and STC8.
 */

#if MCU_FAMILY == 12
	#define COUNTER_MAX 256UL
#else
	#define COUNTER_MAX 65536UL
#endif // MCU_FAMILY == 12

#if MCU_FAMILY == 90
	#pragma save
	// Suppress warning "unreferenced function argument"
	#pragma disable_warning 85
#endif

TimerStatus startTimer(Timer timer, uint32_t sysclkDivisor, OutputEnable enableOutput, InterruptEnable enableInterrupt, CounterControl timerControl) {
	TimerStatus rc = TIMER_FREQUENCY_OK;
	uint8_t sysclkDiv1 = 1;
	
	if (sysclkDivisor == 0) {
		// Too high
		rc = TIMER_FREQUENCY_TOO_HIGH;
	} else {
#ifdef TIMER_HAS_PRESCALERS
		uint16_t prescaler = 0;
		
		switch (timer) {
		case TIMER0:
#ifdef TIMER_HAS_T1
		case TIMER1:
#endif // TIMER_HAS_T1
#endif // TIMER_HAS_PRESCALERS

			if (sysclkDivisor > COUNTER_MAX) {
				if (sysclkDivisor <= (COUNTER_MAX * 12UL)) {
					sysclkDiv1 = 0;
					sysclkDivisor /= 12;
				} else {
					// Too low: T0 and T1 don't have a prescaler.
					rc = TIMER_FREQUENCY_TOO_LOW;
				}
			}

#ifdef TIMER_HAS_PRESCALERS
			break;
		
		default:
			if (sysclkDivisor > COUNTER_MAX) {
				prescaler = sysclkDivisor / COUNTER_MAX;
				
				// Prescalers are 8-bit counters, so PRESCALER_MAX = 256.
				if (prescaler > 256) {
					sysclkDiv1 = 0;
					prescaler /= 12;
					sysclkDivisor /= 12;
				}
				
				// Round the result
				if (sysclkDivisor % prescaler) {
					prescaler++;
				}
				
				sysclkDivisor /= prescaler;
				
				// sysclk is divided by (TMxPS + 1)
				prescaler--;
			}
			break;
		}
		
		switch (timer) {
#ifdef TIMER_HAS_T2
		case TIMER2:
			TM2PS = prescaler;
			break;
#endif // TIMER_HAS_T2

#ifdef TIMER_HAS_T3_T4
		case TIMER3:
			TM3PS = prescaler;
			break;
		
		case TIMER4:
			TM4PS = prescaler;
			break;
#endif // TIMER_HAS_T3_T4
		}
#endif // TIMER_HAS_PRESCALERS
	}

	if (rc == TIMER_FREQUENCY_OK) {
		uint16_t reloadValue = (uint16_t) (COUNTER_MAX - sysclkDivisor);
		
		switch (timer) {
		case TIMER0:
#ifdef M_T0x12
			// Set prescaler
			AUXR = (AUXR & ~M_T0x12) | ((sysclkDiv1 << P_T0x12) & M_T0x12);
#endif
			
			// Configure T0 as a 16-bit auto-reload timer (mode 0).
			TMOD &= 0xf0;
			
			// Configure timer control
			TMOD = (TMOD & ~M_T0_GATE) | ((timerControl << P_T0_GATE) & M_T0_GATE);
			
#if MCU_FAMILY == 12
			// On the STC12, we'll use mode 2 (8-bit auto-reload timer).
			TMOD |= 2;
			
			if (enableOutput == ENABLE_OUTPUT) {
				WAKE_CLKO |= M_T0CLKO;
			} else {
				WAKE_CLKO &= ~M_T0CLKO;
			}
			
			T0H = T0L = reloadValue;
#elif MCU_FAMILY != 90
			if (enableOutput == ENABLE_OUTPUT) {
				INT_CLKO |= M_T0CLKO;
			} else {
				INT_CLKO &= ~M_T0CLKO;
			}
			
			T0 = reloadValue;
#endif
			
			if (enableInterrupt == ENABLE_INTERRUPT) {
				IE1 |= M_T0IE;
			} else {
				IE1 &= ~M_T0IE;
			}
			
			// Clear interrupt flag and start timer
			TCON = (TCON & ~M_T0IF) | M_T0RUN;
			break;
		
#ifdef TIMER_HAS_T1
		case TIMER1:
	#if MCU_FAMILY == 12
			// Configure T1 as an 8-bit auto-reload timer (mode 2)
			TMOD = (TMOD & 0x0f) | (2 << P_T1_MODE);
			
			if (enableOutput == ENABLE_OUTPUT) {
				WAKE_CLKO |= M_T1CLKO;
			} else {
				WAKE_CLKO &= ~M_T1CLKO;
			}
			
			// Set reload value
			T1H = T1L = reloadValue;
	#else
			// Configure T1 as a 16-bit auto-reload timer (mode 0)
			TMOD &= 0x0f;
			
		#if MCU_FAMILY != 90
			if (enableOutput == ENABLE_OUTPUT) {
				INT_CLKO |= M_T1CLKO;
			} else {
				INT_CLKO &= ~M_T1CLKO;
			}
		#endif
			
			// Set reload value
			T1 = reloadValue;
	#endif // MCU_FAMILY == 12
			
			// Configure timer control
			TMOD = (TMOD & ~M_T1_GATE) | ((timerControl << P_T1_GATE) & M_T1_GATE);

	#ifdef M_T1x12
			// Configure prescaling
			AUXR = (sysclkDiv1) ? (AUXR | M_T1x12) : (AUXR & ~M_T1x12);
	#endif
			
			if (enableInterrupt == ENABLE_INTERRUPT) {
				IE1 |= M_T1IE;
			} else {
				IE1 &= ~M_T1IE;
			}
			
			// Clear interrupt flag and start timer
			TCON = (TCON & ~M_T1IF) | M_T1RUN;
			break;
#endif // TIMER_HAS_T1

#if defined(TIMER_HAS_T2) || defined(TIMER_HAS_BRT)
		case TIMER2:
#ifdef TIMER_HAS_T2
			// Configure T2 in timer mode
			AUXR &= ~M_T2_C_T;
			
			if (enableOutput == ENABLE_OUTPUT) {
				INT_CLKO |= M_T2CLKO;
			} else {
				INT_CLKO &= ~M_T2CLKO;
			}
			
			// Set reload value
			T2 = reloadValue;
#endif // TIMER_HAS_T2

#ifdef TIMER_HAS_BRT
			if (enableOutput == ENABLE_OUTPUT) {
				WAKE_CLKO |= M_BRTCLKO;
			} else {
				WAKE_CLKO &= ~M_BRTCLKO;
			}
			
			// Set reload value
			BRT =  reloadValue;
#endif // TIMER_HAS_BRT

			// Configure prescaling
			AUXR = (sysclkDiv1) ? (AUXR | M_T2x12) : (AUXR & ~M_T2x12);
			
#ifdef TIMER_HAS_T2
			if (enableInterrupt == ENABLE_INTERRUPT) {
				IE2 |= M_T2IE;
			} else {
				IE2 &= ~M_T2IE;
			}
			
	#ifdef TIMER_HAS_AUXINTIF
			// Clear interrupt flag
			AUXINTIF &= ~M_T2IF;
	#endif // TIMER_HAS_AUXINTIF
#endif // TIMER_HAS_T2
			
			// Start timer
			AUXR |= M_T2RUN;
			break;
#endif // defined(TIMER_HAS_T2) || defined(TIMER_HAS_BRT)
	
#ifdef TIMER_HAS_T3_T4
		case TIMER3:
			// Configure T3 in timer mode
			T4T3M &= ~M_T3_C_T;
			
			if (enableOutput == ENABLE_OUTPUT) {
				T4T3M |= M_T3CLKO;
			} else {
				T4T3M &= ~M_T3CLKO;
			}
			
			// Configure prescaling
			T4T3M = (sysclkDiv1) ? (T4T3M | M_T3x12) : (T4T3M & ~M_T3x12);
			
			// Set reload value
			T3 = reloadValue;
			
			if (enableInterrupt == ENABLE_INTERRUPT) {
				IE2 |= M_T3IE;
			} else {
				IE2 &= ~M_T3IE;
			}
			
#ifdef TIMER_HAS_AUXINTIF
			// Clear interrupt flag
			AUXINTIF &= ~M_T3IF;
#endif // TIMER_HAS_AUXINTIF
			
			// Start timer
			T4T3M |= M_T3RUN;
			break;
		
		case TIMER4:
			// Configure T4 in timer mode
			T4T3M &= ~M_T4_C_T;
			
			if (enableOutput == ENABLE_OUTPUT) {
				T4T3M |= M_T4CLKO;
			} else {
				T4T3M &= ~M_T4CLKO;
			}
			
			// Configure prescaling
			T4T3M = (sysclkDiv1) ? (T4T3M | M_T4x12) : (T4T3M & ~M_T4x12);
			
			// Set reload value
			T4 = reloadValue;
			
			if (enableInterrupt == ENABLE_INTERRUPT) {
				IE2 |= M_T4IE;
			} else {
				IE2 &= ~M_T4IE;
			}
			
#ifdef TIMER_HAS_AUXINTIF
			// Clear interrupt flag
			AUXINTIF &= ~M_T4IF;
#endif // TIMER_HAS_AUXINTIF
			
			// Start timer
			T4T3M |= M_T4RUN;
			break;
#endif // TIMER_HAS_T3_T4
		}
	}
	
	return rc;
}

#if MCU_FAMILY == 90
	#pragma restore
#endif

#ifdef HAL_TIMER_API_STOP_TIMER
uint16_t stopTimer(Timer timer) {
	uint16_t counterValue = 0;
	
	switch (timer) {
	case TIMER0:
		TCON &= ~M_T0R;
#if MCU_FAMILY == 12
		counterValue = T0L;
#else
		counterValue = T0;
#endif
		break;
	
#ifdef TIMER_HAS_T1
	case TIMER1:
		TCON &= ~M_T1R;
#if MCU_FAMILY == 12
		counterValue = T1L;
#else
		counterValue = T1;
#endif
		break;
#endif // TIMER_HAS_T1

#if defined(TIMER_HAS_T2) || defined(TIMER_HAS_BRT)
	case TIMER2:
		AUXR &= ~M_T2R;
#ifdef TIMER_HAS_T2
		counterValue = T2;
#endif
#ifdef TIMER_HAS_BRT
		counterValue = BRT;
#endif
		break;
#endif // defined(TIMER_HAS_T2) || defined(TIMER_HAS_BRT)

#ifdef TIMER_HAS_T3_T4
	case TIMER3:
		T4T3M &= ~M_T3R;
		counterValue = T3;
		break;
	
	case TIMER4:
		T4T3M &= ~M_T4R;
		counterValue = T4;
		break;
#endif // TIMER_HAS_T3_T4
	}
	
	return counterValue;
}
#endif // HAL_TIMER_API_STOP_TIMER
